"""Simple API class for MongoDB."""
import abc
import os

from bson.objectid import ObjectId
import pymongo


class MongoCollection:
    def __init__(self, uri, db, collection):
        self._client = pymongo.MongoClient(uri)
        self._db = self._client[db]
        self._collection = self._db[collection]

    @property
    def collection(self):
        return self._collection

    def close(self):
        self._client.close()

    def __enter__(self):
        return self.collection

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.close()


class AbstractMongoAPI(abc.ABC):
    def __init__(self, host=None, port=None, user=None, password=None,
                 db=None, collection=None):
        self._host = host or 'mongodb'
        self._port = port or 27017
        self._user = os.getenv('MONGO_INITDB_ROOT_USERNAME', user)
        self._password = os.getenv('MONGO_INITDB_ROOT_PASSWORD', password)
        self._db = os.getenv('MONGO_INITDB_DATABASE', db)
        self._coll = collection or 'memes'

    def _get_uri(self):
        return 'mongodb://%s:%s@%s:%s' % (self._user, self._password,
                                          self._host, self._port)

    @abc.abstractmethod
    def save(self, meme):
        pass

    @abc.abstractmethod
    def get(self, key, val):
        pass

    @abc.abstractmethod
    def get_all(self):
        pass

    @abc.abstractmethod
    def get_by_id(self, str_id):
        pass

    @abc.abstractmethod
    def replace_by_id(self, str_id, new_meme):
        pass


class StatelessMongoAPI(AbstractMongoAPI):
    def __init__(self, host=None, port=None, user=None, password=None,
                 db=None, collection=None):
        super().__init__(host, port, user, password, db, collection)

    def save(self, meme):
        with MongoCollection(self._get_uri(), self._db, self._coll) as memes:
            return str(memes.insert_one(meme).inserted_id)

    def get(self, key, val):
        with MongoCollection(self._get_uri(), self._db, self._coll) as memes:
            return memes.find_one({key: val})

    def get_all(self):
        with MongoCollection(self._get_uri(), self._db, self._coll) as memes:
            return list(memes.find({}))

    def get_by_id(self, str_id):
        mongo_id = ObjectId(str_id)
        return self.get(key='_id', val=mongo_id)

    def replace_by_id(self, str_id, new_meme):
        mongo_id = ObjectId(str_id)
        with MongoCollection(self._get_uri(), self._db, self._coll) as memes:
            memes.replace_one(
                filter={'_id': mongo_id},
                replacement=new_meme,
                upsert=False
            )


class StatefulMongoAPI(AbstractMongoAPI):
    def __init__(self, host=None, port=None, user=None, password=None,
                 db=None, collection=None):
        super().__init__(host, port, user, password, db, collection)
        self._memes = MongoCollection(self._get_uri(), self._db, self._coll)

    def save(self, meme):
        return str(self._memes.collection.insert_one(meme).inserted_id)

    def get(self, key, val):
        return self._memes.collection.find_one({key: val})

    def get_all(self):
        return list(self._memes.collection.find({}))

    def get_by_id(self, str_id):
        mongo_id = ObjectId(str_id)
        return self.get(key='_id', val=mongo_id)

    def replace_by_id(self, str_id, new_meme):
        mongo_id = ObjectId(str_id)
        self._memes.collection.replace_one(
            filter={'_id': mongo_id},
            replacement=new_meme,
            upsert=False
        )

    def close(self):
        self._memes.close()
