import json
from copy import deepcopy
from time import time


class DataModel:
    _required_fields = ['portalId', 'instanceUrl', 'imageUrl', 'numPoints',
                        'numComments', 'removed']

    def __init__(self, model=None):
        if model:
            self._assert_fields_set(model)
            self._model = model
            self._update_in_progress = False
        else:
            self._model = {}
            self._update_in_progress = True

        self._model['history'] = self._model.get('history', [])
        self._model['text'] = self._model.get('text', [])
        self._model['timestamp'] = self._model.get('timestamp', time())
        self._model['removed'] = self._model.get('removed', False)

    def append_text(self, text):
        self._append_to_history_if_update_started()
        self._model['text'].append(text)
        return self

    def set_portal_id(self, portal_id):
        return self._set_value_and_append_to_history('portalId', portal_id)

    def set_instance_url(self, instance_url):
        return self._set_value_and_append_to_history(
            'instanceUrl', instance_url)

    def set_image_template(self, image_template):
        return self._set_value_and_append_to_history(
            'imageTemplate', image_template)

    def set_image_url(self, image_url):
        return self._set_value_and_append_to_history('imageUrl', image_url)

    def set_text(self, text_array):
        return self._set_value_and_append_to_history('text', text_array)

    def set_text_embedding(self, text_embedding):
        return self._set_value_and_append_to_history(
            'textEmbedding', text_embedding)

    def set_image_embedding(self, image_embedding):
        return self._set_value_and_append_to_history(
            'imageEmbedding', image_embedding)

    def set_num_points(self, num_points):
        return self._set_value_and_append_to_history('numPoints', num_points)

    def set_num_comments(self, num_comments):
        return self._set_value_and_append_to_history(
            'numComments', num_comments)

    def set_user_id(self, user_id):
        return self._set_value_and_append_to_history('userId', user_id)

    def set_removed(self, removed):
        return self._set_value_and_append_to_history('removed', removed)

    def commit_and_return_dict(self):
        self._update_in_progress = False
        self._assert_fields_set(self._model)
        return deepcopy(self._model)

    def _set_value_and_append_to_history(self, key, val):
        self._append_to_history_if_update_started()
        self._model[key] = val
        return self

    def _append_to_history_if_update_started(self):
        if self._update_in_progress:
            return
        self._update_in_progress = True
        self._model['history'].append(deepcopy(self._model))
        self._model['timestamp'] = time()

    def _assert_fields_set(self, model):
        if not all(f in model.keys() for f in self._required_fields):
            raise Exception('Not all obligatory fields have been set.')
