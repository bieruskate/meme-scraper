"""Common functions for Celery applications."""
import os

import celery


def make_app(name, task_package_path=None):
    rabbit_user = os.getenv('RABBITMQ_DEFAULT_USER')
    rabbit_pass = os.getenv('RABBITMQ_DEFAULT_PASS')

    if task_package_path:
        task_package_path = [task_package_path]

    app = celery.Celery(
        name,
        broker=f'amqp://{rabbit_user}:{rabbit_pass}@rabbitmq:5672',
        #backend='rpc://',
        include=task_package_path,
    )

    return app
