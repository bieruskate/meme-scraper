"""Celery beat app configuration."""
from celery import signature
from celery.schedules import crontab

from common import celery_common as cc

app = cc.make_app('scraping_scheduler')


@app.on_after_configure.connect
def setup_periodic_tasks(sender, **kwargs):
    sender.add_periodic_task(
        name='Run 9gag hot spider',
        args=('9gag',),
        kwargs={'category': 'hot'},
        schedule=crontab(hour='*/24'),
        sig=signature('run_spider_task'),
        queue='scraping'
    )

    sender.add_periodic_task(
        name='Run 9gag trending spider',
        args=('9gag',),
        kwargs={'category': 'trending'},
        schedule=crontab(hour='*/24'),
        sig=signature('run_spider_task'),
        queue='scraping'
    )

    sender.add_periodic_task(
        name='Run 9gag fresh spider',
        args=('9gag',),
        kwargs={'category': 'fresh'},
        schedule=crontab(hour='*/24'),
        sig=signature('run_spider_task'),
        queue='scraping'
    )

    #######################################

    sender.add_periodic_task(
        name='Run imgflip spider',
        args=('imgflip',),
        schedule=crontab(hour='*/24'),
        sig=signature('run_spider_task'),
        queue='scraping'
    )

    sender.add_periodic_task(
        name='Run memegenerator spider',
        args=('memegenerator',),
        schedule=crontab(hour='*/24'),
        sig=signature('run_spider_task'),
        queue='scraping'
    )

    #######################################

    sender.add_periodic_task(
        name='Run reddit /r/memes spider',
        args=('reddit',),
        kwargs={'subreddit_name': 'memes'},
        schedule=crontab(hour='*/24'),
        sig=signature('run_spider_task'),
        queue='scraping'
    )

    sender.add_periodic_task(
        name='Run reddit /r/dankmemes spider',
        args=('reddit',),
        kwargs={'subreddit_name': 'dankmemes'},
        schedule=crontab(hour='*/24'),
        sig=signature('run_spider_task'),
        queue='scraping'
    )
