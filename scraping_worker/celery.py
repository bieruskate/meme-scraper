from common import celery_common as cc
from scrapers.spiders_runner import run_spider
from scrapers.spiders_runner import run_reddit_spider


app = cc.make_app('scraping_worker')


@app.task(name='run_spider_task')
def run_spider_func(portal_name, **additional_kwargs):
    if portal_name == 'reddit':
        run_reddit_spider(**additional_kwargs)
    else:
        run_spider(portal_name, 'scrapers', additional_kwargs)
