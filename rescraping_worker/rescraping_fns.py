"""Functions for rescraping memes."""
import json
import os
import re

import requests
import requests_html as rh


# def _example_rescraping_func(url):
#     # if the meme was deleted -> return None
#     # else -> return (totalPoints, nbComments)
#     return None


def rescraping_fn_ninegag(url):
    with rh.HTMLSession() as sess:
        res = sess.get(url)

        if res.status_code != 200:
            return None

        # NOTE(pbielak): When loading a particular meme page, it is not fully
        # rendered, so we need to use a headless browser to render JavaScript
        # parts - here we use `requests_html` library, which handles it for us.
        res.html.render()

        points = res.html.find('a.point')[0].text
        points = int(points[:-len(' points')])

        nb_comments = res.html.find('a.comment')[0].text
        nb_comments = int(nb_comments[:-len(' comments')])

        return points, nb_comments


def rescraping_fn_imgflip(url):
    with rh.HTMLSession() as sess:
        res = sess.get(url)

        if res.status_code != 200:
            return None

        nb_comments = len(list(res.html.find('div.com')))

        stats_str = res.html.find('.img-views')[0].text
        m = re.search(r'(\d+) upvotes', stats_str)
        if m:
            points = int(m.group(1).replace(',', ''))
            return points, nb_comments

        raise RuntimeError('Could not extract totalPoints and comments')


def rescraping_fn_memegenerator(url):
    api_key = os.getenv('MEMEGENERATOR_API_KEY', 'demo')

    instance_id = url.split('/')[-1]
    instance_api_url = f'http://version1.api.memegenerator.net//Instance_Select?' \
        f'instanceID={instance_id}&apiKey={api_key}'

    response = requests.get(instance_api_url)

    if response.status_code != 200:
        return None

    meme = json.loads(response.text)['result']

    points = meme['totalVotesScore']
    nb_comments = meme['commentsCount']

    return points, nb_comments
