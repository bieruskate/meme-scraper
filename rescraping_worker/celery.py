from common import celery_common as cc
from common.data_model import DataModel
from common import db_api

from rescraping_worker import rescraping_fns as fns


A = -2
B = 21

DEFAULT_DELAY_IN_HOURS = 10
MIN_DELAY_IN_HOURS = 1
MAX_DELAY_IN_HOURS = 18


app = cc.make_app('scraping_worker')


@app.task(name='rescraping_task')
def rescraping_func(mongo_id, portal_name, initial_call=False):
    if portal_name == '9gag':
        parse_url = fns.rescraping_fn_ninegag
    elif portal_name == 'imgflip':
        parse_url = fns.rescraping_fn_imgflip
    # elif portal_name == 'reddit':
    #     pass
    # ...
    else:
        raise ValueError('Bad portal name!')

    if initial_call:
        rescraping_delay_in_hours = DEFAULT_DELAY_IN_HOURS
    else:
        mongo_api = db_api.StatelessMongoAPI()
        meme_info = mongo_api.get_by_id(mongo_id)
        if meme_info is None:
            return mongo_id

        new_meme_metadata = parse_url(meme_info['instanceUrl'])
        if new_meme_metadata is None:
            new_meme_info = (DataModel(meme_info)
                .set_removed(True)
                .commit_and_return_dict()
            )
            mongo_api.replace_by_id(mongo_id, new_meme_info)
            return mongo_id

        new_num_points, new_num_comments = new_meme_metadata

        num_comments_diff = new_num_comments - \
                            meme_info['numComments']
        num_comments_diff = max(0, num_comments_diff)

        rescraping_delay_in_hours = A * num_comments_diff + B
        rescraping_delay_in_hours = max(
            MIN_DELAY_IN_HOURS, rescraping_delay_in_hours)

        new_meme_info = (DataModel(meme_info)
            .set_num_points(new_num_points)
            .set_num_comments(new_num_comments)
            .commit_and_return_dict()
        )
        mongo_api.replace_by_id(mongo_id, new_meme_info)

    if rescraping_delay_in_hours <= MAX_DELAY_IN_HOURS:
        rescraping_func.apply_async(
            args=(mongo_id, portal_name),
            countdown=rescraping_delay_in_hours * 60 ** 2
        )

    return mongo_id
