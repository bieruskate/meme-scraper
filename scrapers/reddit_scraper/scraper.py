from celery import group
import praw

from common import celery_common as cc
from common import data_model
from common import db_api
from .config import CREDENTIALS


def ensure_credentials_filled(creds):
    for k, v in creds.items():
        if not v:
            raise RuntimeError(
                f'Found empty credential field: "{k}". To use the Reddit '
                f'scraper, you must provide all credential fields.'
            )


def scrap_reddit(subreddit_name, limit=None):
    ensure_credentials_filled(CREDENTIALS)

    app = cc.make_app('reddit-spider')
    mongo_api = db_api.StatefulMongoAPI()

    reddit = praw.Reddit(
        client_id=CREDENTIALS['CLIENT_ID'],
        client_secret=CREDENTIALS['CLIENT_SECRET'],
        user_agent=CREDENTIALS['USER_AGENT'],
        username=CREDENTIALS['LOGIN'],
        password=CREDENTIALS['PASSWORD']
    )

    subreddit = reddit.subreddit(subreddit_name)

    for submission in subreddit.new(limit=limit):
        if not submission.url.endswith(('.jpg', '.png')):
            continue

        meme = data_model.DataModel()

        (meme
         .set_portal_id('reddit')
         .set_instance_url('https://www.reddit.com' + submission.permalink)
         .set_image_url(submission.url)
         .set_user_id(submission.author.name)
         .set_num_points(submission.ups - submission.downs)
         .set_num_comments(len(submission.comments.list()))
         )

        meme = meme.commit_and_return_dict()

        if mongo_api.get('instanceUrl', meme['instanceUrl']):
            print(f'[Reddit] Meme {meme} already in DB!')
            continue

        mongo_id = mongo_api.save(meme)

        rescraping_sig = app.signature(
            'rescraping_task',
            args=(mongo_id, 'reddit'),
            kwargs={'initial_call': True},
            queue='rescraping'
        )

        ocr_sig = app.signature(
            'OcrImageFromUrlTask',
            args=(mongo_id,),
            queue='ocr'
        )

        group(ocr_sig, rescraping_sig).apply_async()

    app.close()
    mongo_api.close()
