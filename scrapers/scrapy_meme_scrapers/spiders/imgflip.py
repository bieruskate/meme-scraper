import re
from urllib import parse

import scrapy

from common.data_model import DataModel


class ImgflipSpider(scrapy.Spider):
    name = 'imgflip'
    start_urls = [
        'https://imgflip.com/',
    ]

    meme_text_regexp = re.compile(r'.*? \| (.*?) \|')
    stats_regexp = re.compile(r'(.+) views, (.+) upvotes')

    def __init__(self, *args, max_page=10000, **kwargs):
        self.max_page = int(max_page)
        super().__init__(*args, **kwargs)

    def parse(self, response):
        # Get memes from current site
        meme_urls = response.css(
            'a.base-img-link'
        )

        for meme_url in meme_urls:
            yield response.follow(meme_url, self.parse_meme)

        # Proceed with next site
        next_site_link = response.css('a.pager-next')
        if next_site_link:
            yield response.follow(next_site_link[0], self.parse)
        else:
            parsed_url = list(parse.urlparse(response.url))
            current_page = int(parse.parse_qs(parsed_url[4])['page'][0])
            next_page = current_page + 1
            if next_page > self.max_page:
                return
            parsed_url[4] = parse.urlencode({'page': next_page})
            yield scrapy.Request(parse.urlunparse(parsed_url), self.parse)

    def parse_meme(self, response):
        text = self._process_text(response)
        _, points = self._extract_views_and_points_from_meme(response)

        nb_comments = len(list(response.css('div.com')))

        meme = DataModel()

        (meme
         .set_portal_id(ImgflipSpider.name)
         .set_instance_url(response.url)
         .set_image_url(response.css('img#im::attr(src)').get())
         .set_text([text])
         .set_num_points(points)
         .set_num_comments(nb_comments)
         .set_user_id(response.css('.img-views .u-username::text').get())
         )

        return meme.commit_and_return_dict()

    def _extract_views_and_points_from_meme(self, response):
        stats_str = response.css('.img-views::text').extract_first()
        m = self.stats_regexp.match(stats_str)
        if m:
            stats = m.group(1), m.group(2)
            return [int(stat.replace(',', '')) for stat in stats]
        return None, None

    def _process_text(self, response):
        texts = response.css('img#im::attr(alt)').getall()
        text = texts[0]
        m = self.meme_text_regexp.match(text)
        if m:
            return m.group(1)
        return ''

