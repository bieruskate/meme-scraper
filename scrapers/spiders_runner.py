import os
from contextlib import contextmanager

from billiard import Process
from scrapy.crawler import CrawlerProcess
from scrapy.utils.project import get_project_settings

from scrapers.reddit_scraper.scraper import scrap_reddit


@contextmanager
def _temporarily_change_dir(path):
    owd = os.getcwd()
    os.chdir(path)
    yield
    os.chdir(owd)


def run_spider(portal_name, scrapy_project_path='.', additional_kwargs=None):
    if additional_kwargs is None:
        additional_kwargs = {}

    if portal_name == '9gag':
        crawl_args = ('9gag',)
        crawl_kwargs = {'category': additional_kwargs.get('category', 'hot')}
    else:
        raise ValueError('Bad portal name!')

    def f():
        with _temporarily_change_dir(scrapy_project_path):
            process = CrawlerProcess(get_project_settings())

        process.crawl(*crawl_args, **crawl_kwargs)
        process.start()

    p = Process(target=f, daemon=False)
    p.start()
    p.join()


def run_reddit_spider(subreddit_name='memes'):
    scrap_reddit(subreddit_name)
