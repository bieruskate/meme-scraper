"""Celery app definition for DB workers."""
from common import celery_common as cc


app = cc.make_app(name='db', task_package_path='db.tasks')


if __name__ == '__main__':
    app.start()
