"""Celery app definition for OCR workers."""
from common import celery_common as cc


app = cc.make_app(name='ocr', task_package_path='ocr.tasks')


if __name__ == '__main__':
    app.start()
